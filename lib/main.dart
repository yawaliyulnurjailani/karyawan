import 'package:flutter/material.dart';
import 'package:karyawan/example/1_ui_column_container.dart';
import 'package:karyawan/example/2_ui_row_container.dart';
import 'package:karyawan/example/3_stack_container.dart';
import 'package:karyawan/example/4_ui_wrap_container.dart';
import 'package:karyawan/example/5_ui_expanded_row.dart';
import 'package:karyawan/example/6_ui_expanded_column.dart';
import 'package:karyawan/example/7_ui_listview_group.dart';
import 'package:karyawan/example/8_ui_login_example.dart';
import 'package:karyawan/screens/form_karyawan.dart';
import 'package:karyawan/screens/home.dart';
import 'package:karyawan/screens/home_card.dart';
import 'package:karyawan/screens/login.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/util/life_cycle_event_handler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppstate createState() => _MyAppstate();
}

class _MyAppstate extends State<MyApp> {
  bool databaseIsReady = false;
  LifecycleEventHandler _lifecycleEventHandler;

  @override
  void initState() {
    super.initState();
    AppService.open().then((database) {
      setState(() {
        databaseIsReady = true;
      });
    });
    _lifecycleEventHandler = LifecycleEventHandler(onResume: (state) async {
      return AppService.open().then((database) {
        setState(() {
          databaseIsReady = true;
        });

        return databaseIsReady;
      });
    }, onSuspend: (state) async {
      return AppService.close().then((val) {
        setState(() {
          databaseIsReady = false;
        });

        return databaseIsReady;
      });
    });
    WidgetsBinding.instance.addObserver(_lifecycleEventHandler);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(_lifecycleEventHandler);
    AppService.close().then((val) {
      setState(() {
        databaseIsReady = false;
      });
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('databaseIs ready? $databaseIsReady');
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Karyawan',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: databaseIsReady
          ? Login()
          : Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
