import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:karyawan/interface/karyawan_interface.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/model/server_response.dart';

import 'package:karyawan/util/logging_interceptors.dart';

class KaryawanRepository implements IKaryawanRepository {
  Dio get dio => _dio();
  Dio _dio() {
    final options = BaseOptions(
      baseUrl: "http://222.124.139.166:5007/api/flutter",
      connectTimeout: 5000,
      receiveTimeout: 3000,
      contentType: "application/json;charset=utf-8",
    );
    var dio = Dio(options);
    dio.interceptors.add(LoggingInterceptors());
    return dio;
  }

  @override
  Future<int> create(Karyawan karyawan) async {
    try {
      var response = await dio.post("/saveKaryawan", data: karyawan);
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(jsonEncode(response.data));
        ServerResponse value = ServerResponse.fromJson(jsonData);
        return value.data;
      } else {
        return 0;
      }
    } catch (error, stacktrace) {
      throw Exception("Exception occured: $error stackTrace: $stacktrace");
    }
  }

  @override
  Future<int> delete(int id) async {
    try {
      var response = await dio.post("/deleteKaryawan",
          data: {"id": id},
          options: Options(contentType: Headers.formUrlEncodedContentType));
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(jsonEncode(response.data));
        ServerResponse value = ServerResponse.fromJson(jsonData);
        return value.data;
      } else {
        return 0;
      }
    } catch (error, stacktrace) {
      throw Exception("Exception occured: $error stackTrace: $stacktrace");
    }
  }

  @override
  Future<List<Karyawan>> findAll([String searchCriteria]) async {
    try {
      var response = await dio
          .get("/getAllKaryawan", queryParameters: {"search": searchCriteria});
      if (response.statusCode == 200) {
        final List rawData = jsonDecode(jsonEncode(response.data['data']));
        List<Karyawan> list = rawData.map((f) => Karyawan.fromJson(f)).toList();
        return list;
      } else {
        return null;
      }
    } catch (error, stacktrace) {
      throw Exception("Exception occured: $error stackTrace: $stacktrace");
    }
  }

  @override
  Future<Karyawan> findByID(int id) async {
    try {
      var response = await dio.get("/getByID", queryParameters: {"id": id});
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(jsonEncode(response.data['data']));
        Karyawan karyawan = Karyawan.fromJson(jsonData);
        return karyawan;
      } else {
        return null;
      }
    } catch (error, stacktrace) {
      throw Exception("Exception occured: $error stackTrace: $stacktrace");
    }
  }

  @override
  Future<int> update(int id, Karyawan model) {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<ServerResponse> doLogin(String userName, String password) async {
    try {
      var response = await dio.post("/doLogin",
          data: {"userName": userName,"password":password},
          options: Options(contentType: Headers.formUrlEncodedContentType));
      if (response.statusCode == 200) {
        final jsonData = jsonDecode(jsonEncode(response.data));
        ServerResponse value = ServerResponse.fromJson(jsonData);
        return value;
      } else {
        return null;
      }
    } catch (error, stacktrace) {
      throw Exception("Exception occured: $error stackTrace: $stacktrace");
    }
  }

}
