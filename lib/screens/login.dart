import 'package:flutter/material.dart';
import 'package:karyawan/model/server_response.dart';
import 'package:karyawan/screens/home_card.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/widget/progress_button.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _userFocus, _passwordFocus;
  String _user, _pass;

  @override
  void initState() {
    super.initState();
    _userFocus = FocusNode();
    _passwordFocus = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _userFocus?.dispose();
    _passwordFocus?.dispose();
    _formKey.currentState?.dispose();
  }

  _showSnackBar(messages) {
    final snackbar = SnackBar(
      content: Text(messages),
      backgroundColor: Colors.red,
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Future<ServerResponse> fetchUser(String user, String pass) async {
    return AppService.karyawanService.doLogin(user, pass);
  }

  void homeScreen(AnimationController controller) async {
    controller.forward();
    print("delay start");
    await Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) {
            return HomeCard();
          },
        ),
      );
    });
    print("delay stop");
    controller.reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(title: Text('Login')),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(50),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 190,
                            height: 190,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        "https://i.imgur.com/Jvh1OQm.jpg"))),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30, bottom: 30),
                            child: Text(
                              "Login Your Account",
                              textScaleFactor: 2,
                            ),
                          ),
                          TextFormField(
                            validator: (value) {
                              if (value.isEmpty || value.length == 0) {
                                return "User tidak boleh Kosong !";
                              }
                              return null;
                            },
                            focusNode: _userFocus,
                            onSaved: (String value) {
                              _user = value;
                            },
                            onFieldSubmitted: (term) {
                              _passwordFocus.unfocus();
                              FocusScope.of(context)
                                  .requestFocus(_passwordFocus);
                            },
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                labelText: "Username",
                                hintText: "Masukkan Username",
                                icon: Icon(Icons.account_circle_outlined)),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            keyboardType: TextInputType.visiblePassword,
                            validator: (value) {
                              if (value.isEmpty || value.length == 0) {
                                return "Password tidak boleh Kosong !";
                              }
                              return null;
                            },
                            onSaved: (String value){
                              _pass = value;
                            },
                            decoration: InputDecoration(
                                labelText: "Password",
                                hintText: "Masukkan Password",
                                icon: Icon(Icons.vpn_key)),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            height: 50,
                            child: ProgressButton(
                              borderRadius: BorderRadius.all(Radius.circular(28)),
                              strokeWidth: 2,
                              child: Text(
                                "Login",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                              onPressed: (AnimationController controller) async {
                                final form = _formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  fetchUser(_user, _pass).then((value) => {
                                    if (value.data != 0)
                                      {
                                        _showSnackBar(value.status)

                                      }else {
                                      homeScreen(controller)
                                    }
                                  });
                                }

                              },
                            ),

                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Text(
                                  "Lupa Password",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}