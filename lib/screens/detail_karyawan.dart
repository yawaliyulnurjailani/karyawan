import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/util/capitalize.dart';

import 'form_karyawan.dart';

class DetailKaryawan extends StatefulWidget {
  int id;

  DetailKaryawan({Key key, @required this.id}) : super(key: key);

  @override
  _DetailKaryawanState createState() => _DetailKaryawanState();
}

class _DetailKaryawanState extends State<DetailKaryawan> {
  Karyawan karyawan;
  @override
  void initState() {
    fetchData();
    super.initState();
  }

  fetchData() {
    return AppService.karyawanService.findKaryawan(id: widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Karyawan"),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => FormKaryawan(
                        title: "Edit Karyawan",
                        isEdit: true,
                        data: karyawan,
                        id: karyawan.id,
                      )),
                ).then((value) => {setState(() {})});
              })
        ],
      ),
      body: Center(
        child: FutureBuilder (
          future: fetchData(),
          builder: (context, AsyncSnapshot snapshot) {
            karyawan = snapshot.data;
            if (karyawan == null) {
              return new CircularProgressIndicator();
            }
            return ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Image.network(
                    'http://222.124.139.166:5007/Kry/4440.bmp',
                    width: 150,
                    height: 150,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.people),
                  title: Text(karyawan.fullName),
                  subtitle: Text("Nama Lengkap"),
                ),
                ListTile(
                  leading: Icon(Icons.phone),
                  title: Text(karyawan.mobilePhone),
                  subtitle: Text("Nomor Hp"),
                ),
                ListTile(
                  leading: Icon(Icons.tag_faces),
                  title: Text(karyawan.gender),
                  subtitle: Text("Jenis Kelamin"),
                ),
                ListTile(
                  leading: Icon(Icons.grade),
                  title: Text(karyawan.grade.toUpperCase()),
                  subtitle: Text("Jenjang"),
                ),
                ListTile(
                  leading: Icon(Icons.map),
                  title: Text(karyawan.address),
                  subtitle: Text("Alamat"),
                ),
                ListTile(
                  leading: Icon(Icons.favorite),
                  title: Text(karyawan.pengalamanList
                      .map((String e) => capitalize(e))
                      .join(', ')),
                  subtitle: Text("Pengalaman"),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
