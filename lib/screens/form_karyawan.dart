import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/util/capitalize.dart';
import 'package:karyawan/widget/check_box.dart';
import 'package:karyawan/widget/form_label.dart';
import 'package:karyawan/widget/radio_button.dart';

class FormKaryawan extends StatefulWidget {
  String title;
  final bool isEdit;
  final Karyawan data;
  final int id;

  FormKaryawan(
      {Key key,
      @required this.title,
      @required this.isEdit,
      this.data,
      this.id})
      : super(key: key);

  @override
  _FormKaryawanState createState() => _FormKaryawanState();
}

class _FormKaryawanState extends State<FormKaryawan> {
  static const genders = Karyawan.genders;
  static const grades = Karyawan.grades;
  static const pengalamans = Karyawan.pengalamanKerja;

  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  double genPadding = 16.0;
  TextEditingController _firstNameCtrl,
      _mobilePhoneCtrl,
      _lastNameCtrl,
      _addressCtrl;

  FocusNode _firstNameFocus, _mobilePhoneFocus, _lastNameFocus, _addressFocus;
  String _firstName, _lastName, _mobilePhone, _address, _gender, _grade;
  List<String> pengalamanList = [];

  final List<DropdownMenuItem<String>> _gradeItems = grades
      .map((String val) => DropdownMenuItem<String>(
            value: val,
            child: Text(val.toUpperCase()),
          ))
      .toList();

  @override
  void initState() {
    // TODO: implement initState
    _gender = 'PRIA';
    _firstNameFocus = FocusNode();
    _mobilePhoneFocus = FocusNode();
    _lastNameFocus = FocusNode();

    _firstNameCtrl = TextEditingController();
    _mobilePhoneCtrl = TextEditingController();
    _lastNameCtrl = TextEditingController();
    _addressCtrl = TextEditingController();
    if (widget.data != null && widget.data is Karyawan && widget.isEdit) {
      _firstName = widget.data.firstName;
      _lastName = widget.data.lastName;
      _mobilePhone = widget.data.mobilePhone;
      _address = widget.data.address;
      _gender = widget.data.gender;
      _grade = widget.data.grade;
      pengalamanList = widget.data.pengalamanList;
      String data = widget.data.pengalamanList.toString();
      print('pengalaman $data');
      _firstNameCtrl.value = TextEditingValue(
          text: widget.data.firstName,
          selection:
          TextSelection.collapsed(offset: widget.data.firstName.length));
      _lastNameCtrl.value = TextEditingValue(
          text: widget.data.lastName,
          selection:
          TextSelection.collapsed(offset: widget.data.lastName.length));
      _mobilePhoneCtrl.value = TextEditingValue(
          text: widget.data.mobilePhone,
          selection:
          TextSelection.collapsed(offset: widget.data.mobilePhone.length));
      _addressCtrl.value = TextEditingValue(
          text: widget.data.address,
          selection:
          TextSelection.collapsed(offset: widget.data.address.length));
      super.initState();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _firstNameFocus?.dispose();
    _mobilePhoneFocus?.dispose();
    _lastNameFocus?.dispose();
    _addressFocus?.dispose();
    _formKey.currentState?.dispose();
    _firstNameCtrl?.dispose();
    _mobilePhoneCtrl?.dispose();
    _lastNameCtrl?.dispose();
    super.dispose();
  }

  _showSnackBar(message) {
    final snackbar = SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(),
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.only(
                top: 20.0,
                left: 8.0,
                right: 8.0,
                bottom: 8.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Card(
                    elevation: 8,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  child: TextFormField(
                                    controller: _firstNameCtrl,
                                    validator: (value) {
                                      if (value.isEmpty || value.length == 0) {
                                        return "first name is not null";
                                      }
                                      return null;
                                    },
                                    focusNode: _firstNameFocus,
                                    textInputAction: TextInputAction.next,
                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: "First Name",
                                    ),
                                    onSaved: (String value) {
                                      print("onsave first name $value");
                                      _firstName = value;
                                    },
                                    onFieldSubmitted: (term) {
                                      _firstNameFocus.unfocus();
                                      FocusScope.of(context)
                                          .requestFocus(_lastNameFocus);
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Expanded(
                                child: Container(
                                  child: TextFormField(
                                    controller: _lastNameCtrl,
                                    validator: (value) {
                                      if (value.isEmpty || value.length == 0) {
                                        return "last name is not null";
                                      }
                                      return null;
                                    },
                                    focusNode: _lastNameFocus,
                                    textInputAction: TextInputAction.next,
                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: "Last Name",
                                    ),
                                    onSaved: (String value) {
                                      print("onsave last name $value");
                                      _lastName = value;
                                    },
                                    onFieldSubmitted: (term) {
                                      _lastNameFocus.unfocus();
                                      FocusScope.of(context)
                                          .requestFocus(_mobilePhoneFocus);
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: genPadding,
                          ),
                          TextFormField(
                            controller: _mobilePhoneCtrl,
                            validator: (value) {
                              if (value.isEmpty || value.length == 0) {
                                return "mobile phone is not null";
                              } else if (value.length > 12) {
                                return "mobile phone tidak boleh lebih dari 12";
                              }
                              return null;
                            },
                            focusNode: _mobilePhoneFocus,
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 20.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(28),
                              ),
                              labelText: "Mobile Phone",
                            ),
                            onSaved: (String value) {
                              print("onsave mobile phone $value");
                              _mobilePhone = value;
                            },
                            onFieldSubmitted: (term) {},
                          ),
                          SizedBox(
                            height: genPadding,
                          ),
                          FormLabel("Gender"),
                          Row(
                            children: genders
                                .map(
                                  (String val) => RadioButton<String>(
                                    value: val,
                                    groupValue: _gender,
                                    label: Text(
                                      capitalize(val),
                                    ),
                                    onChange: (String value) {
                                      setState(
                                        () {
                                          _gender = value;
                                          print("grade $_gender");
                                        },
                                      );
                                    },
                                  ),
                                )
                                .toList(),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, left: 5.0),
                            child: DropdownButton(
                              value: _grade,
                              hint: Text("Chosee Grades"),
                              items: _gradeItems,
                              isExpanded: true,
                              onChanged: (String value) {
                                setState(
                                  () {
                                    _grade = value;
                                    print("grade $_grade");
                                  },
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: genPadding,
                          ),
                          FormLabel("Works Experience"),
                          Column(
                            children: pengalamans
                                .map(
                                  (String val) => CheckBox(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 1.0),
                                    value: pengalamanList.contains(val),
                                    onChanged: (bool value) {
                                      setState(
                                        () {
                                          if (pengalamanList.contains(val)) {
                                            pengalamanList.remove(val);
                                          } else {
                                            pengalamanList.add(val);
                                          }
                                          print(pengalamanList);
                                        },
                                      );
                                    },
                                    label: capitalize(val),
                                  ),
                                )
                                .toList(),
                          ),
                          SizedBox(
                            height: genPadding,
                          ),
                          TextFormField(
                            controller: _addressCtrl,
                            validator: (value) {
                              if (value.isEmpty || value.length == 0) {
                                return "Can't Empty !";
                              }
                              return null;
                            },
                            maxLines: 3,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "address",
                            ),
                            onSaved: (String value) {
                              print("onSave Alamat $value");
                              _address = value;
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.save,
          color: Colors.white,
        ),
        onPressed: () {
          final form = _formKey.currentState;
          if (form.validate()) {
            form.save();
            Karyawan karyawan = Karyawan();
            if (_gender == null) {
              _showSnackBar("Gender is not null !");
            } else if (_grade == null) {
              _showSnackBar("Grade is not null !");
            } else if (pengalamanList.length == 0) {
              _showSnackBar("Experience is not null !");
            } else {
              karyawan.firstName = _firstName;
              karyawan.lastName = _lastName;
              karyawan.mobilePhone = _mobilePhone;
              karyawan.grade = _grade;
              karyawan.gender = _gender;
              karyawan.pengalamanList = pengalamanList;
              karyawan.address = _address;
              final onSuccess = (int idx) => Navigator.pop(context, true);
              final onError =
                  (Object error) => _showSnackBar('Tidak bisa simpan data');
              if (widget.isEdit) {
                karyawan.id = widget.id;
              } else {
                karyawan.id = 0;
              }
              AppService.karyawanService
                  .createKaryawan(karyawan)
                  .then(onSuccess)
                  .catchError(onError);
            }
          }
        },
      ),
    );
  }
}
