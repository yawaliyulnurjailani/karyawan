import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/screens/detail_karyawan.dart';
import 'package:karyawan/screens/form_karyawan.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/util/capitalize.dart';
import 'package:karyawan/util/random.dart';

class HomeCard extends StatefulWidget {
  @override
  _HomeCardState createState() => _HomeCardState();
}

class _HomeCardState extends State<HomeCard> {
  List<Karyawan> _karyawan = [];
  String _searchCriteria = '';
  bool _showTextField = false;
  bool _shouldWidgetUpdate = true;

  fecthData() {
    if (!_shouldWidgetUpdate) {
      return Future.value(_karyawan);
    }
    return AppService.karyawanService.findAllKaryawan(_searchCriteria);
  }

  @override
  void initState() {
    // TODO: implement initState
    fecthData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: _showTextField
            ? TextField(
          // expands: true,
          decoration: InputDecoration(
            hintText: 'Cari Karyawan',
            fillColor: Colors.white,
            filled: true,
          ),
          autofocus: true,
          onSubmitted: (value) {
            setState(() {
              _searchCriteria = value;
              _shouldWidgetUpdate = true;
            });
          },
        ) : Text('Karyawan'),
        actions: <Widget>[
          IconButton(
              icon: _showTextField ? Icon(Icons.close) : Icon(Icons.search),
              onPressed: () {
                setState(() {
                  _showTextField = !_showTextField;
                  _shouldWidgetUpdate = false;
                  if (!_showTextField) {
                    _searchCriteria = "";
                    _shouldWidgetUpdate = true;
                    fecthData();
                  }
                });
              }),
        ],
      ),
      body: FutureBuilder<List<Karyawan>>(
        future: fecthData(),
        initialData: <Karyawan>[],
        builder: (context, AsyncSnapshot snapshot){
          if (snapshot.data == null) {
            return Center(child: CircularProgressIndicator());
          }

          return ListView.builder(
            itemCount: snapshot.hasData ? snapshot.data.length : 0,
            itemBuilder: (BuildContext context, int i) {
              final Karyawan karyawan = snapshot.data[i];
              return Padding(
                padding: const EdgeInsets.all(14.0),
                child: Card(
                  color: Colors.grey[0],
                  child: Padding(
                    padding: const EdgeInsets.all(14),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) {
                                      return DetailScreen(
                                        id: i,
                                      );
                                    },
                                  ),
                                );
                              },
                              child: Padding(
                                padding:
                                EdgeInsets.only(top: 10, bottom: 10, right: 10),
                                child: Container(
                                  width: 75,
                                  height: 75,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.fill,
                                        image: NetworkImage(
                                            "http://222.124.139.166:5007/Kry/444$i.bmp")),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    karyawan.firstName,
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(karyawan.address),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(karyawan.gender),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    karyawan.grade,
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(karyawan.mobilePhone),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(Karyawan.pengalamanKerja[getRandom(4)]),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text("Are you sure?"),
                                      content:
                                      Text("Delete data ${karyawan.firstName}"),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () {
                                            setState(
                                                  () {
                                                AppService.karyawanService
                                                    .deleteKaryawanByID(id: karyawan.id);
                                                Navigator.pop(context);
                                              },
                                            );
                                          },
                                          child: Text("Yes"),
                                        ),
                                        FlatButton(
                                          onPressed: () {
                                            setState(
                                                  () {
                                                Navigator.pop(context);
                                              },
                                            );
                                          },
                                          child: Text("No"),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              child: Text(
                                "Delete",
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        DetailKaryawan(key: ValueKey(i), id: karyawan.id),
                                  ),
                                ).then((value) => {setState(() {})});
                              },
                              child: Text(
                                "Detail",
                                style: TextStyle(color: Colors.green),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => FormKaryawan(
                title: "add Karyawan",
                isEdit: false,
              ),
            ),
          ).then((value) => {setState(() {})});
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  int id;

  DetailScreen({@required this.id});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Image"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: "imageHero",
            child: Image.network("http://222.124.139.166:5007/Kry/444$id.bmp"),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
