import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/screens/detail_karyawan.dart';
import 'package:karyawan/screens/form_karyawan.dart';
import 'package:karyawan/service/app_service.dart';
import 'package:karyawan/util/capitalize.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Karyawan> _karyawan;

  void fecthData() {
    // _karyawan = AppService.karyawanService.findAllKaryawan();
  }

  @override
  void initState() {
    // TODO: implement initState
    fecthData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("App Karyawan"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => FormKaryawan(
                    title: "add Karyawan",
                  ),
                ),
              ).then((value) => {setState(() {})});
            },
          ),
        ],
      ),
      body: ListView.separated(
        separatorBuilder: (BuildContext context, int i) => Divider(
          color: Colors.grey[400],
        ),
        itemCount: _karyawan.length,
        itemBuilder: (BuildContext context, int i) {
          final Karyawan karyawan = _karyawan[i];
          return ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      DetailKaryawan(key: ValueKey(i), id: i),
                ),
              ).then((value) => {setState(() {})});
            },
            leading: Image.asset(
                "assets/images/${karyawan.gender.toLowerCase()}.png"),
            // leading: Image.asset("assets/images/"+karyawan.gender.toLowerCase()+".png"),
            title: Text(karyawan.fullName),
            subtitle: Text(capitalize(karyawan.gender)),
            trailing: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(karyawan.grade),
                Text(karyawan.mobilePhone),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => FormKaryawan(
                title: "add Karyawan",
              ),
            ),
          ).then((value) => {setState(() {})});
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
