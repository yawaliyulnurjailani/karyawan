import 'package:karyawan/domain/domain.dart';
import 'package:karyawan/util/capitalize.dart';
import 'package:karyawan/util/enums.dart';

class Karyawan {
  static const genders = ['PRIA', 'WANITA'];
  static const grades = ['TK', 'SD', 'SMP', 'SMA'];
  static const pengalamanKerja = [
    'PROGRAMMER',
    'ARSITEK',
    'ENGINEER',
    'TUKANG LAS'
  ];
  int id;
  String firstName;
  String lastName;
  String mobilePhone;
  String gender;
  String grade;
  List<String> pengalamanList = [];
  String address;

  String get fullName => '${capitalize(firstName)} ${capitalize(lastName)}';

  Karyawan(
      {this.id,
      this.firstName,
      this.lastName,
      this.mobilePhone,
      this.gender,
      this.pengalamanList,
      this.grade,
      this.address});

  factory Karyawan.fromJson(Map<String, dynamic> map) => Karyawan(
      id: map["id"],
      firstName: map["firstName"],
      lastName: map["lastName"],
      mobilePhone: map["mobilePhone"],
      gender: map["gender"],
      pengalamanList:
          map['pengalaman'] is String ? map['pengalaman'].split(',') : [],
      grade: map["grade"],
      address: map["address"]);

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "firstName": firstName,
      "lastName": lastName,
      "mobilePhone": mobilePhone,
      "gender": gender,
      "pengalaman": pengalamanList.join(','),
      "grade": grade,
      "address": address
    };
  }
}
