class ServerResponse {
  String status;
  int data;

  ServerResponse({this.status, this.data});

  factory ServerResponse.fromJson(Map<String, dynamic> map) =>
      ServerResponse(status: map["status"], data: map['data']);
}