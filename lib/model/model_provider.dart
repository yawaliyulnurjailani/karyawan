import 'dart:io';
import 'package:path/path.dart';
import 'package:karyawan/model/model.dart';
import 'package:sqflite/sqflite.dart';

ModelProvider _modelProvider;

class ModelProvider {
  static ModelProvider getInstance() {
    if (_modelProvider != null && _modelProvider is ModelProvider)
      return _modelProvider;
    _modelProvider = ModelProvider('_karyawan_flutter.db', 1);
    return _modelProvider;
  }

  final String dbName;
  final int dbVersion;
  Database _database;

  ModelProvider(this.dbName, this.dbVersion);

  Future<Database> open() async {
    var databasePath = await getDatabasesPath();
    try {
      await Directory(databasePath).create(recursive: true);
    } catch (_) {
      print('directory already exits');
    }
    final path = join(databasePath, dbName);
    final model = Model();
    _database = await openDatabase(
      path,
      version: dbVersion,
      onConfigure: model.onConfigure,
      onCreate: model.onCreate,
      onUpgrade: model.onUpgrade,
      onDowngrade: model.onDowngrade,
    );
    return _database;
  }

  Future<void> close () {
    return getDatabase().then((db) => db.close());
  }

  Future<Database> getDatabase() async {
    if(_database == null) throw 'Error open database';
    return _database;
  }

}
