import 'package:faker/faker.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/util/random.dart';

seedKaryawan () {
  var faker = new Faker();
  var karyawan = new Karyawan();
  karyawan.firstName = faker.person.firstName();
  karyawan.lastName = faker.person.lastName();
  karyawan.mobilePhone = '188';
  karyawan.gender = Karyawan.genders[getRandom(2)];
  karyawan.grade = Karyawan.grades[getRandom(4)];
  karyawan.address = faker.address.streetAddress();
  karyawan.pengalamanList = Karyawan.pengalamanKerja;

  return karyawan;
}

generateKaryawans ([int max = 5]) {

  List<Karyawan> list = [];
  for(var i=0; i < max;i++){
    list.add(seedKaryawan());
  }

  return list;

}