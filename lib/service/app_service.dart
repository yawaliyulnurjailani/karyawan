import 'package:karyawan/model/model_provider.dart';
import 'package:karyawan/repository/karyawan_repository.dart';
import 'package:karyawan/service/karyawan_service.dart';
import 'package:sqflite/sqflite.dart';

KaryawanRepository _karyawanRepository = KaryawanRepository();
final KaryawanService _karyawanService = KaryawanService(_karyawanRepository);

class AppService{

  static KaryawanService get karyawanService => _karyawanService;

  static Future<Database> open () {
    return ModelProvider.getInstance().open();
  }

  static Future<void> close() {
    return ModelProvider.getInstance().close();
  }

}