import 'package:karyawan/interface/karyawan_interface.dart';
import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/model/server_response.dart';

class KaryawanService{

  final IKaryawanRepository repository;
  const KaryawanService(this.repository);

  Future<int> createKaryawan (Karyawan model) {
    return repository.create(model);
  }

  Future<List<Karyawan>> findAllKaryawan(String searchCriteria){
    return repository.findAll(searchCriteria);
  }

  Future<Karyawan> findKaryawan({int id}){
    return repository.findByID(id);
  }

  Future<int> deleteKaryawanByID({int id}){
    return repository.delete(id);
  }

  Future<int> updateKaryawan ({int id,Karyawan model}) {
    return repository.update(id,model);
  }

  Future<ServerResponse> doLogin(String user,String pass) {
    return repository.doLogin(user, pass);
  }

}