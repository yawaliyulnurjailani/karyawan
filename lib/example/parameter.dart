void main() {
  // parameter with opsional
  String say(String from, String msg,[String hp = "IOS"]){

    var result = "$from say $msg";
    if(hp != null){
      result = "$result with $hp";
    }

    return result;

  }

  var say1 = say("Gojek", "Pesanan Sesuai Aplikasi");
  var say2 = say("Grab", "Pesanan Sesuai Aplikasi", "Android");
  print(say1);
  print(say2);

}