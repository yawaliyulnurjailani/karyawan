import 'package:flutter/material.dart';

class ColumnContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // header
      // body
      appBar: AppBar(
        title: Text("Column Container"),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.yellow,
            ),
          ],
        ),
      ),
    );
  }
}
