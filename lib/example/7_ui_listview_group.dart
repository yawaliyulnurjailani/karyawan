import 'package:flutter/material.dart';

class ListViewGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListView Group"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "List 1",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
