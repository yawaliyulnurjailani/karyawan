import 'package:flutter/material.dart';

class WrapContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wrap Container"),
      ),
      body: Wrap(
        children: <Widget>[
          Container(
            width: 100,
            height: 100,
            color: Colors.red,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.green,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.yellow,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.purple,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.orange,
          ),
        ],
      ),
    );
  }
}
