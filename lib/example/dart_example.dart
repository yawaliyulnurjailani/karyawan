void main(){
  exampleString();
}

//string
void exampleString(){
  var nama      = "Yawaliyul";
  var belakang  = 'Nur Jailani';
  var helloDart = '''
  Selamat datang
  Training flutter
  Training dart
  ''';
  print(nama+" "+belakang);
  print(helloDart);
}

// boolean
void getBoolean(){
  bool isRidwan         = true;
  var isWay             = false;
  final bool isFLutter  = false;
  const bool isSmart    = false;
  print(isRidwan);
}

//integer
void getInteger(){
  var numOne = 50;
}