import 'package:flutter/material.dart';

class ExpandedRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Expanded Row"),
      ),
      body: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 5, top: 5, bottom: 5),
              height: 100,
              color: Colors.red,
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 5, bottom: 5),
              height: 100,
              color: Colors.green,
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 5, top: 5, bottom: 5),
              height: 100,
              color: Colors.yellow,
            ),
          ),
        ],
      ),
    );
  }
}
