void main() {
  var yawaliyul = new Karyawan("Yawaliyul", DateTime.parse('2000-01-12'));
  yawaliyul._namaBelakang = "Nur Jailani";
  print(yawaliyul.nama);
  print(yawaliyul.getNamaBelakang);
  print(yawaliyul.getUmur);
}

class Karyawan {

  final String nama;
  final DateTime umur;
  String _namaBelakang;

  Karyawan(this.nama, this.umur);

  int get getUmur {
    var todayYear = new DateTime.now().year;
    return todayYear - this.umur.year;
  }

  String get getNamaBelakang => this._namaBelakang;
  set setNamaBelakang (String val) => this._namaBelakang = val;



}