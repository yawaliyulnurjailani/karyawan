void main(){

  var list = [1,2,3,4,5];
  var karyawan = ['Yawaliyul','comingSoon','not fould'];
  print(list);

  // looping dengan map
  var newList = list.map((x) => x * 2 ).toList();
  print(newList);

  // looping foreach
  for(var item in karyawan){
    print(item);
  }

  // looping increment
  for(var i=0;i<karyawan.length;i++){
    print(karyawan[i]);
  }
}