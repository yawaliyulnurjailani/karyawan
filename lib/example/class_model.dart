void main(){
  var namaLengkap = new Karyawan("Yawaliyul", "Nur Jailani");
  print(namaLengkap.hello());
  print(namaLengkap.umur);
}

class Karyawan extends Umur {
  String namaDepan;
  String namaBelakang;
  Karyawan(this.namaDepan, this.namaBelakang);
  hello() {
    return "Hello ${getNamaLengkap()}";
  }

  getNamaLengkap(){
    return "$namaDepan $namaBelakang";
  }
}

class Umur {
  int umur = 20;

  getUmur(){
    return "$umur";
  }
}