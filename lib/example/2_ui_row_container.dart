import 'package:flutter/material.dart';

class RowContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //header
      //body
      appBar: AppBar(
        title: Text("Row Container"),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.yellow,
            ),
          ],
        ),
      ),
    );
  }
}
