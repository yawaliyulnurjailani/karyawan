void main() {
  getMap();
  getArray();
}

void getMap(){
  var karyawan = {
    'nama' : 'Yawaliyul',
    'nama_belakang' : 'Nur Jailani',
    'usia' : 20
  };

  print(karyawan['nama']);
}

void getArray(){
  var karyawan = ['Yawaliyul', 'comingSoon', 'fould'];
  print(karyawan[1]);
}