void main() {
  
  var karyawan = {
    1:"Yawaliyul",
    2:"ComingSoon",
    3:"not fould"
  };
  
  // function boolean
  
  bool isKaryawan(int number){
    return karyawan[number] != null;
  }
  
  // function no data Type
  
  isKaryawanName(int number) {
    return karyawan[number];
  }
  
  // inline function
  
  bool inLineKaryawan (int number) => karyawan != null;
  print(isKaryawan(1));
  print(isKaryawanName(1));
  print(inLineKaryawan(2));
  
}