import 'package:flutter/material.dart';

class ClassExpandedColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Expanded Column"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              height: 100,
              color: Colors.red,
            ),
            Expanded(
              child: Container(
                width: 100,
                color: Colors.yellow,
              ),
            ),
            Container(
              height: 100,
              color: Colors.green,
            ),
          ],
        ),
      ),
    );
  }
}
