import 'package:flutter/material.dart';

class RadioButton<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final ValueChanged<T> onChange;
  final Color activeColor;
  final MaterialTapTargetSize materialTapTargetSize;
  final Widget label;

  const RadioButton(
      {Key key,
      @required this.value,
      @required this.groupValue,
      @required this.onChange,
      this.activeColor,
      this.materialTapTargetSize,
      @required this.label})
      : assert(value != null),
        assert(groupValue != null),
        assert(onChange != null),
        assert(label != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Radio<T>(
          key: key,
          value: value,
          groupValue: groupValue,
          onChanged: (T newValue) {
            onChange(newValue);
          },
          activeColor: activeColor,
          materialTapTargetSize: materialTapTargetSize,
        ),
        label,
      ],
    );
  }
}
