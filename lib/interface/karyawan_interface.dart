import 'package:karyawan/model/karyawan.dart';
import 'package:karyawan/model/server_response.dart';

abstract class IKaryawanRepository {
  Future<int> create(Karyawan karyawan);
  Future<List<Karyawan>> findAll([String searchCriteria]);
  Future<Karyawan> findByID (int id);
  Future<int> delete(int id);
  Future<int> update(int id, Karyawan model);
  Future<ServerResponse> doLogin(String userName,String password);
}